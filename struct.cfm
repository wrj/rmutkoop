<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Struct - Structure</title>
</head>
<body>
    
<cfset StudentCard = {
                        "Name" = "John Doe",
                        "Faculty" = "Management",
                        "StudentID" = "24424324"
                     } />
<cfdump var = "#StudentCard#" />
    
<cfoutput>#StudentCard.StudentID#</cfoutput><br>

<cfset StudentCard.StudentID = 54234234 >    
<cfoutput>#StudentCard.StudentID#</cfoutput><br>
    
<cfset StudentCard.Email = "jedt3d@gmail.com">
<cfdump var="#StudentCard#">

<cfset StudentCard.Mobiles = ["0836000064","0923100019"]>
<cfdump var="#StudentCard#">

</body>
</html>