<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Array</title>
</head>
<body>
    <cfset StudentCard = [  "John Doe",
                            "Faculty of Management",
                            54234233 ] >
    <cfoutput>
    StudentCard Array size is : #ArrayLen(StudentCard)#
    </cfoutput>
    <cfdump var = "#StudentCard#" label="##1" >
    
    <cfset StudentCard[2] = "Faculty of Science" >
    <cfdump var = "#StudentCard#" label="##2" >
    
    <cfset ArrayAppend(StudentCard,"sjedt@gmail.com") >
    <cfset ArrayAppend(StudentCard,"0836000064") >
    <cfdump var = "#StudentCard#" label="##3" >
    
    <cfset ArrayDeleteAt(StudentCard, 4 ) >
    <cfdump var = "#StudentCard#" label="##4" >

    <cfset StudentCard[5] = "Male" >
    <cfdump var = "#StudentCard#" label="##5" >
    
</body>
</html>